public class ParStanja {
		
	int lijevo_stanje, desno_stanje;
	
	public ParStanja(int lijevo_stanje, int desno_stanje) {
		this.lijevo_stanje = lijevo_stanje;
		this.desno_stanje = desno_stanje;
	}

	public int getLijevo_stanje() {
		return lijevo_stanje;
	}

	public void setLijevo_stanje(int lijevo_stanje) {
		this.lijevo_stanje = lijevo_stanje;
	}

	public int getDesno_stanje() {
		return desno_stanje;
	}

	public void setDesno_stanje(int desno_stanje) {
		this.desno_stanje = desno_stanje;
	}
}
