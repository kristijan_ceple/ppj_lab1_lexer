#!/bin/bash

echo "Point 1"
echo $PWD

dir_array=("svaki_drugi_a2" "svaki_drugi_a1" "simplePpjLang" "nadji_a1" "nadji_a2" "minusLang" "greske_u_stanju" "minusLang_laksi" "minusLang_tezi" "nadji_x" "nadji_x_oporavak" "nadji_x_retci" "poredak" "ppjLang_laksi" "ppjLang_tezi" "regex_escapes" "regex_laksi" "regex_regdefs" "regex_tezi" "simplePpjLang_laksi" "simplePpjLang_tezi" "state_hopper" "svaki_treci_x" "vrati_se" "vrati_se_prioritet" "duljina")

echo "Point 2"

Testing_folder=$PWD
for dir in ${dir_array[*]}
do
    current="$Testing_folder/data/lab1/tests/$dir"

    cat $current/test.lan | java -cp $Testing_folder/target/classes GLA
    cd analizator

    cat $current/test.in | java -cp $Testing_folder/target/classes/analizator LA > $current/output.out
    cd $Testing_folder

done
