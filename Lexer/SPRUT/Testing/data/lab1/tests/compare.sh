#!/bin/bash

dir_array=("svaki_drugi_a2" "svaki_drugi_a1" "simplePpjLang" "nadji_a1" "nadji_a2" "minusLang" "greske_u_stanju" "minusLang_laksi" "minusLang_tezi" "nadji_x" "nadji_x_oporavak" "nadji_x_retci" "poredak" "ppjLang_laksi" "ppjLang_tezi" "regex_escapes" "regex_laksi" "regex_regdefs" "regex_tezi" "simplePpjLang_laksi" "simplePpjLang_tezi" "state_hopper" "svaki_treci_x" "vrati_se" "vrati_se_prioritet" "duljina")

for dir in ${dir_array[*]}
do
	echo "Currently testing $dir"

	res=$(diff $dir/output.out $dir/test.out)
	if [ -z "$res" ]
	then
		printf "$dir Test successful\n\n"
	else
		printf "$dir Test failed\n\n"
		printf "############################ FAIL ################################\n\n"
	fi
done
