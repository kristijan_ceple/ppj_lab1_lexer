import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author kikyy99
 *
 */
class EnfaSimpleTest {
	
	Enfa testEnfa;
	
	@BeforeEach
	void setUpBeforeEach() {
		testEnfa = new Enfa();
		
		// Adding states
		Map<String, Boolean> states = new HashMap<>();
		states.put("p1", false);
		states.put("p2", false);
		states.put("p3", false);
		states.put("p4", true);
		states.put("p5", false);
		testEnfa.addStates(states);
		testEnfa.setStartingState("p1");
		
		// Setting the alphabet
		testEnfa.addAlphabetChar("a", "b");
		
		// Setting the transmissions
		Map<String, String[]> transMap = new HashMap<>();		//Map for Enfa.Automaton.State p1
		transMap.put("a", new String[]{"p3"});
		transMap.put("b", new String[]{"p2"});
		testEnfa.addTransitions("p1", transMap);
		testEnfa.addTransition("p2", Enfa.EPSILON, new String[] {"p4"});
		testEnfa.addTransition("p3", Enfa.EPSILON, new String[] {"p5"});
	}
	
	@Test
	void EnfaTest() {
		// Let's make sure the actual structure is okay
		TreeSet<Enfa.Automaton.State> states = (TreeSet<Enfa.Automaton.State>)testEnfa.getStates();		// Want them sorted, so I can check alphabetically
		Enfa.Automaton.State startingState = testEnfa.getStartingState();
		
		// Let's first test the getStartingState function
		assertEquals(startingState.toString(), "p1");
		
		// Now it's time to test the other states(the structure of the Enfa.Automaton)
		Enfa.Automaton.State currState = states.first();
		assertEquals(testEnfa.getEmptyState().toString(), currState.toString());		// The empty Enfa.Automaton.State - means en error has occured in the input sequence - DEF NOT ACCEPTABLE
		
		currState = states.higher(currState);
		assertEquals("p1", currState.toString());
		
		currState = states.higher(currState);
		assertEquals("p2", currState.toString());
		
		currState = states.higher(currState);
		assertEquals("p3", currState.toString());
		
		currState = states.higher(currState);
		assertEquals("p4", currState.toString());
		
		currState = states.higher(currState);
		assertEquals("p5", currState.toString());
		
		// Now let's test the alphabet
		Set<Enfa.AlphabetChar> alphabetSet = testEnfa.getAlphabet();
		TreeSet<Enfa.AlphabetChar> sortedAlphabetSet = new TreeSet<Enfa.AlphabetChar>();
		sortedAlphabetSet.addAll(alphabetSet);
		
		Enfa.AlphabetChar current = sortedAlphabetSet.first();
		assertEquals(new Enfa.AlphabetChar(Enfa.EPSILON), current);
		
		current = sortedAlphabetSet.higher(current);
		assertEquals(new Enfa.AlphabetChar("a"), current);
		
		current = sortedAlphabetSet.higher(current);
		assertEquals(new Enfa.AlphabetChar("b"), current);
		
		// Transitions shall be tested in another test method
	}
	
	@Test
	void performSingleTransitionTestNotAccepted() {
		testEnfa.performSingleTransition("a");
		assertEquals(false, testEnfa.isAccepted());
		
		// Let's check the destination states - they should be p3 and p5
		Set<Enfa.Automaton.State> activeStates = testEnfa.getActiveStates();
		assertEquals(2, activeStates.size());
		
		Set<String> expectedStateNames = new HashSet<String>();
		expectedStateNames.add("p3");
		expectedStateNames.add("p5");

		// Now let's check if the enfa really is in those states
		for(Enfa.Automaton.State currState : activeStates) {
			String name = currState.toString();
			assertTrue(expectedStateNames.contains(name));
		}
	}
	
	@Test
	void performSingleTransitionTestAccepted() {
		testEnfa.performSingleTransition("b");
		assertEquals(true, testEnfa.isAccepted());
		
		// Let's check the destination states - they should be p3 and p5
				Set<Enfa.Automaton.State> activeStates = testEnfa.getActiveStates();
				assertEquals(2, activeStates.size());
				
				Set<String> expectedStateNames = new HashSet<String>();
				expectedStateNames.add("p2");
				expectedStateNames.add("p4");

				// Now let's check if the enfa really is in those states
				for(Enfa.Automaton.State currState : activeStates) {
					String name = currState.toString();
					assertTrue(expectedStateNames.contains(name));
				}
	}
	
	@Test
	void isAcceptedTest() {
		assertEquals(false, testEnfa.isAccepted());		// Tests whether the initial Enfa.Automaton.State is acceptable(it shouldn't be)
	}
}
