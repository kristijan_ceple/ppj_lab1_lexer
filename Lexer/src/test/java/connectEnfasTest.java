import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class connectEnfasTest {

	public static Enfa e1;
	public static Enfa e2;
	public static Enfa e3;
	public static Enfa e4;
	
//	@BeforeEach
//	void setUp() throws Exception {
//		// Not yet needed
//	}

//	@BeforeAll
//	static void setUpBeforeClass() throws Exception {
//		// Add code here
//	}
	
	@BeforeEach
	void setUp() throws Exception {
		e1 = new Enfa();
		e2 = new Enfa();
		e3 = new Enfa();
		e4 = new Enfa();
		
		e1.addState("p1", false);		// The basis ENFA is ready
		e1.setStartingState("p1");
		
		// The second ENFA
		e2.addState("p2", false);
		e2.addState("p3", true);
		e2.addTransition("p2", Enfa.EPSILON, "p3");
		e2.setStartingState("p2");
		
		// The third ENFA
		Map<String, Boolean> states = new HashMap<>();
		states.put("p4", false);
		states.put("p5", false);
		states.put("p6", true);
		states.put("p7", false);
		states.put("p8", false);
		e3.addStates(states);
		e3.setStartingState("p4");
		
		// Setting the alphabet
		e3.addAlphabetChar("a", "b");
		
		// Setting the transmissions
		Map<String, String[]> transMap = new HashMap<>();		//Map for State p1
		transMap.put("a", new String[]{"p7"});
		transMap.put("b", new String[]{"p5"});
		e3.addTransitions("p4", transMap);
		e3.addTransition("p5", Enfa.EPSILON, new String[] {"p6"});
		e3.addTransition("p7", Enfa.EPSILON, new String[] {"p8"});
		
		// The fourth ENFA
		// Adding states
		states.clear();
		e4.addState("p9", false);
		e4.setStartingState("p9");		// Let's set the starting State
		
		states.put("p10", false);
		states.put("p11", false);
		states.put("p12", true);
		e4.addStates(states);
		
		// Setting the alphabet
		e4.addAlphabetChar("a", "b");
		
		// Setting the transmissions
		transMap.clear();
		
		// Map for p9
		transMap.put(Enfa.EPSILON, new String[] {"p10", "p12"});
//		transMap.put(Enfa.EPSILON, new String[] {"p12"});
		e4.addTransitions("p9", transMap);	// For p1
		
		// Map for p10
		transMap.clear();
		transMap.put("a", new String[]{"p10"});
		transMap.put("b", new String[]{"p11"});
		e4.addTransitions("p10", transMap);
		
		// Map for p11
		transMap.clear();
		transMap.put("a", new String[]{"p11"});
		transMap.put(Enfa.EPSILON, new String[]{"p12"});
		e4.addTransitions("p11", transMap);
		
		// p12 has got no transitions - end-state
		// Now connect the enfas into one
		e1.connectEnfas(e2);
		e1.connectEnfas(e3);
		e1.connectEnfas(e4);
		
		// Prepare the Automaton for operation
		e1.prepare();
	}

	@Test
	void connectEnfasMethodTest1() {
		// Let's test the method using strings "b" and "" and then later more complicated
		String toTest = "";			// First an empty String
		e1.setInput(new String[] {toTest});
		assertTrue(e1.isAccepted());
		
		List<String> activeStates = e1.getActiveStatesList();
		assertTrue(activeStates.size()==7);
		assertTrue(activeStates.contains("p1"));
		assertTrue(activeStates.contains("p2"));
		assertTrue(activeStates.contains("p3"));
		assertTrue(activeStates.contains("p4"));
		assertTrue(activeStates.contains("p9"));
		assertTrue(activeStates.contains("p12"));
		assertTrue(activeStates.contains("p10"));
	}
	
	@Test
	void connectEnfasMethodTest2() {
		// Let's test the method using strings "b" and "" and then later more complicated
		String toTest = "b";			// First an empty String
		e1.setInput(new String[] {toTest});
		e1.performWholeSequenceTransition();
		assertTrue(e1.isAccepted());
		
		// p11, p12, p5, p6
		List<String> activeStates = e1.getActiveStatesList();
		assertTrue(activeStates.size()==4);
		assertTrue(activeStates.contains("p11"));
		assertTrue(activeStates.contains("p12"));
		assertTrue(activeStates.contains("p5"));
		assertTrue(activeStates.contains("p6"));
	}
	
	@Test
	void connectEnfasMethodTest3() {
		// Let's test the method using strings "b" and "" and then later more complicated
		String toTest = "a";			// First an empty String
		e1.setInput(new String[] {toTest});
		e1.performWholeSequenceTransition();
		assertFalse(e1.isAccepted());
		
		// p11, p12, p5, p6
		List<String> activeStates = e1.getActiveStatesList();
		assertTrue(activeStates.size()==3);
		assertTrue(activeStates.contains("p10"));
		assertTrue(activeStates.contains("p7"));
		assertTrue(activeStates.contains("p8"));
	}
	
	@Test
	void connectEnfasMethodTest4() {
		// Let's test the method using strings "b" and "" and then later more complicated
		String[] toTest = {"a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"
				, "a", "a", "b"};
		e1.setInput(toTest);
		e1.performWholeSequenceTransition();
		assertTrue(e1.isAccepted());
		
		// p11, p12, p5, p6
		List<String> activeStates = e1.getActiveStatesList();
		assertTrue(activeStates.size()==2);
		assertTrue(activeStates.contains("p11"));
		assertTrue(activeStates.contains("p12"));
	}
	
	@Test
	void connectEnfasMethodTest5() {
		// Let's test the method using strings "b" and "" and then later more complicated
		String[] toTest = {"b", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"
				, "a", "a", "a"};
		e1.setInput(toTest);
		e1.performWholeSequenceTransition();
		assertTrue(e1.isAccepted());
		
		// p11, p12, p5, p6
		List<String> activeStates = e1.getActiveStatesList();
		assertTrue(activeStates.size()==2);
		assertTrue(activeStates.contains("p11"));
		assertTrue(activeStates.contains("p12"));
	}
	
	@Test
	void connectEnfasMethodTest6() {
		// Let's test the method using strings "b" and "" and then later more complicated
		String[] toTest = {"a", "a", "a", "b", "a", "a", "a", "a", "a", "a", "a"
				, "a", "a", "a"};
		e1.setInput(toTest);
		e1.performWholeSequenceTransition();
		assertTrue(e1.isAccepted());
		
		// p11, p12, p5, p6
		List<String> activeStates = e1.getActiveStatesList();
		assertTrue(activeStates.size()==2);
		assertTrue(activeStates.contains("p11"));
		assertTrue(activeStates.contains("p12"));
	}
}
