import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class RegexTest {
	
	Regex regex = new Regex();

	@Test
	void pronadiDesnuZagraduTest() {
		String izraz = "ab(d(sd)k())";
		int pocetniIndex = 2;
		
		assertEquals(Regex.pronadiDesnuZagradu(izraz, pocetniIndex), 11);
		assertEquals(Regex.pronadiDesnuZagradu(izraz, 4), 7);
		assertEquals(Regex.pronadiDesnuZagradu(izraz, 9), 10);
		
		izraz = "((()))";
		assertEquals(Regex.pronadiDesnuZagradu(izraz, 0), 5);
		assertEquals(Regex.pronadiDesnuZagradu(izraz, 1), 4);
		assertEquals(Regex.pronadiDesnuZagradu(izraz, 2), 3);
		
		izraz = "\\(()\\)()";		// Actually written \(()/)()
		assertEquals(Regex.pronadiDesnuZagradu(izraz, 2), 3);
		assertEquals(Regex.pronadiDesnuZagradu(izraz, 6), 7);
	}
	
	@Test
	void novo_stanjeTest() {
		Enfa testEnfa = new Enfa();
		regex.novo_stanje(testEnfa);
		regex.novo_stanje(testEnfa);
		regex.novo_stanje(testEnfa);
		
		assertTrue(testEnfa.containsState("0"));
		assertTrue(testEnfa.containsState("1"));
		assertTrue(testEnfa.containsState("2"));
	}
	
	@Test
	void je_operatorTest() {
		String expression = "(";
		assertTrue(Regex.je_operator(expression, 0));
		
		expression = "\\(";
		assertFalse(Regex.je_operator(expression, 1));
		assertTrue(Regex.je_operator(expression, 0));
		
		expression = "\\\\(";
		assertTrue(Regex.je_operator(expression, 2));
		assertFalse(Regex.je_operator(expression, 1));
		assertTrue(Regex.je_operator(expression, 0));
		
		expression = "\\\\\\(";
		assertFalse(Regex.je_operator(expression, 3));
		assertTrue(Regex.je_operator(expression, 2));
		assertFalse(Regex.je_operator(expression, 1));
		assertTrue(Regex.je_operator(expression, 0));
		
		expression = "\\";
		assertTrue(Regex.je_operator(expression, 0));
		
		expression = "\\\\";
		assertFalse(Regex.je_operator(expression, 1));
		assertTrue(Regex.je_operator(expression, 0));
		
		expression = "\\\\\\";
		assertTrue(Regex.je_operator(expression, 2));
		assertFalse(Regex.je_operator(expression, 1));
		assertTrue(Regex.je_operator(expression, 0));
	}

//	@Test
//	void pretvoriTest() {
//		String expression = "abc";
//		Enfa testEnfa = new Enfa();
//		ParStanja statesPair = regex.pretvori(expression, testEnfa);
//		
////		System.out.println(statesPair.getLijevo_stanje());
////		System.out.println(statesPair.getDesno_stanje());
////		System.out.println(testEnfa.getStartingState());
////		System.out.println(testEnfa.getStates());
////		
////		expression = "abc";
////		testEnfa = new Enfa();
////		
////		statesPair = regex.pretvori(expression, testEnfa);
////		System.out.println(statesPair.getLijevo_stanje());
////		System.out.println(statesPair.getDesno_stanje());
////		System.out.println(testEnfa.getStartingState());
////		System.out.println(testEnfa.getStates());
//		
//		// Let's check if they correspond
//		testEnfa.setInput(new String[]{"a", "b", "c"});
//		testEnfa.addAlphabetChar("a", "b", "c");
//		testEnfa.setStartingState("0");
//		testEnfa.performWholeSequenceTransition();
//		assertTrue(testEnfa.isAccepted());
//	}
	
}
