import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;

class ConversionAlgoTest {

	@Test
	void testConstructEnfa() {
		String regex = "abc*";
		String testString = "abccccc";
		
		Enfa enfa = ConversionAlgo.constructEnfa(regex);
		
		boolean tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "ab";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "abc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "a";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "abcb";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "abca";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "abcd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "abcdasdas";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
	}
	
	@Test
	void regexTest1() {
		String regex = "(1|2)";
		Enfa enfa = ConversionAlgo.constructEnfa(regex);
		
		String testString = "";
		boolean tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
	}
	
	@Test
	void regexTest2() {
		String regex = "(1|2|3|4|5)|a|bcccd*";
		Enfa enfa = ConversionAlgo.constructEnfa(regex);
		
		String testString = "";
		boolean tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "1";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "2";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "3";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "a";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1ccc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "2ccc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "3ccc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "4ccc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "5ccc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "4cccd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "4cccdddddd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "4cccdd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "bccc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "bcccd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "bcccdddddd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "bcccdd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "bcc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "bc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "rubbish";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
	}

	@Test
	void regexTest3() {
		String regex = "t*";
		Enfa enfa = ConversionAlgo.constructEnfa(regex);
		String toTest;
		
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "t";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ttttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ta";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ta1";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1t";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tat";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "t ";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = " t";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "22|";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "\\|";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	}
	
	@Test
	void regexTest4() {
		// Here we shall test escaping sequences :D
		String regex = "\\_|\\\\|\\||yomama";
		Enfa enfa = ConversionAlgo.constructEnfa(regex);
		String toTest;
		
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = " ";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "|";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "yomama";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "\\";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "  ";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "\\\\";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "|||";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "||";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "\\\\\\\\";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "    ";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = " yomama";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "yomaama";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	}
	
	@Ignore
	void testUnifyEnfas() {
		String regex1 = "a|b";
		String regex2 = "abc";
		String regex3 = "1|2";
		String regex4 = "123*";
		
		int counter = 0;
		Enfa enfa1 = ConversionAlgo.constructEnfa(regex1, counter);
		counter = enfa1.getLastStateNumber();
		Enfa enfa2 = ConversionAlgo.constructEnfa(regex2, counter++);
		counter = enfa2.getLastStateNumber();
		Enfa enfa3 = ConversionAlgo.constructEnfa(regex3, counter++);
		counter = enfa3.getLastStateNumber();
		Enfa enfa4 = ConversionAlgo.constructEnfa(regex4, counter++);

		Enfa enfa = ConversionAlgo.unifyEnfas(enfa1, enfa2, enfa3, enfa4);
		
		String testString = "a";
		boolean tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "2";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "abc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "123";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1233333";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1233";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "12333334";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "1234";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "ab";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "ac";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "13";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "3";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		String toTest = "a";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "b";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "aab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abb";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abc";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "ac";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "aab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abb";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "2";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "112";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "122";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "3";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "123";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1233";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12333";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "123333";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "2";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "121";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "122";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1231";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1232";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "|";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "t";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ttttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ta";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ta1";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1t";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tat";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "t ";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = " t";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "22|";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "\\|";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	}

	@Test
	void debugHelpTestAndRegexTest3() {
		String regex1 = "a|b";
		String regex2 = "abc";
		String regex3 = "1|2";
		String regex4 = "123*";
		
		Enfa regex1enfa = ConversionAlgo.constructEnfa(regex1);
		String toTest = "a";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex1enfa, toTest));
		
		toTest = "b";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex1enfa, toTest));
		
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex1enfa, toTest));
		
		toTest = "aab";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex1enfa, toTest));
	
		toTest = "abb";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex1enfa, toTest));
		
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex1enfa, toTest));
		
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex1enfa, toTest));
	
		Enfa regex2enfa = ConversionAlgo.constructEnfa(regex2);
		toTest = "abc";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
		
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
	
		toTest = "ac";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
	
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
	
		toTest = "aab";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
	
		toTest = "abb";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
	
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
	
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
		
		toTest = "cc";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
		
		toTest = "ccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
		
		toTest = "cccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex2enfa, toTest));
	
		Enfa regex3enfa = ConversionAlgo.constructEnfa(regex3);
		toTest = "1";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex3enfa, toTest));
		
		toTest = "2";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex3enfa, toTest));
		
		toTest = "12";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex3enfa, toTest));
		
		toTest = "112";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex3enfa, toTest));
	
		toTest = "122";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex3enfa, toTest));
		
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex3enfa, toTest));
		
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex3enfa, toTest));
		
		toTest = "3";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex3enfa, toTest));
		
		Enfa regex4enfa = ConversionAlgo.constructEnfa(regex4);
		toTest = "123";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
		
		toTest = "12";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
	
		toTest = "1233";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
		
		toTest = "12333";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
		
		toTest = "123333";
		assertTrue(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
		
		toTest = "1";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
	
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
	
		toTest = "2";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
	
		toTest = "121";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
	
		toTest = "122";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
	
		toTest = "1231";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
		
		toTest = "1232";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
		
		toTest = "ccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
		
		toTest = "cccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(regex4enfa, toTest));
	}
	
	@Test
	void testUnifyRegexes1() {
		String regex1 = "a|b";
		String regex2 = "1|2";
		String regex3 = "abc";
		String regex4 = "123*";
		
		Enfa enfa = ConversionAlgo.unifyRegexes(regex1, regex2, regex3, regex4);
		
		String testString = "a";
		boolean tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "2";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "abc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "123";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1233333";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1233";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "12333334";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "1234";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "ab";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "ac";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "13";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "3";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		String toTest = "a";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "b";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "aab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abb";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abc";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "ac";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "aab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abb";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "2";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "112";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "122";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "3";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "123";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1233";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12333";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "123333";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "2";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "121";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "122";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1231";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1232";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	}
	
	@Test
	void testUnifyRegexes3() {
		String regex1 = "a|b";
		String regex2 = "1|2";
		String regex3 = "abc";
		
		Enfa enfa = ConversionAlgo.unifyRegexes(regex1, regex2, regex3);
		
		String testString = "a";
		boolean tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "abc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "c";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "d";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "cd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "2";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "ab";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "abcd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "ac";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "13";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "3";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
	}
	
	@Test
	void testUnifyRegexes2() {
		String regex1 = "a|b";
		String regex2 = "1|2";
		String regex3 = "c|d";
		
		Enfa enfa = ConversionAlgo.unifyRegexes(regex1, regex2, regex3);
		
		String testString = "a";
		boolean tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "c";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "d";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "cd";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "2";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "ab";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "ac";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "13";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "3";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
	}
	
	@Test
	void testUnifyRegexes4() {
		String regex1 = "a|b";
		String regex2 = "1|2";
		String regex3 = "abc";
		String regex4 = "123*";
		String regex5 = "t*";
		String regex6 = "\\|";
		
		Enfa enfa = ConversionAlgo.unifyRegexes(regex1, regex2, regex3, regex4, regex5, regex6);
		
		String testString = "a";
		boolean tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "b";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "2";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "abc";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "123";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1233333";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "1233";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "12333334";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "1234";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "ab";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "12";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertTrue(tmp);
		
		testString = "ac";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "13";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		testString = "3";
		tmp = RegexEnfa.testRegexOnEnfa(enfa, testString);
		assertFalse(tmp);
		
		String toTest = "a";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "b";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "aab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abb";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abc";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "ac";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "aab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "abb";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "ab";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "2";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "112";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "122";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "c";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "3";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "123";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1233";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "12333";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "123333";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "2";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "121";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "122";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	
		toTest = "1231";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1232";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "cccc";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "|";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "t";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ttttt";
		assertTrue(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ta";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "ta1";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "1t";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "tat";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "t ";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = " t";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "22|";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
		
		toTest = "\\|";
		assertFalse(RegexEnfa.testRegexOnEnfa(enfa, toTest));
	}
	
	@Test
	void regexTest5() {
		String regex = "'(\\(|\\)|\\{|\\}|\\||\\*|\\\\|\\$|\\_|!|\"|#|%|&|+|,|-|.|/|0|1|2|3|4|5|6|7|8|9|:|;|<|=|>|?|@|A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z|[|]|^|_|`|a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|w|x|y|z|~)'";
		String toTest = "''";
		Enfa enfa = ConversionAlgo.constructEnfa(regex);
		boolean res = RegexEnfa.testRegexOnEnfa(enfa, toTest);
		
		assertFalse(res);
	}
}
