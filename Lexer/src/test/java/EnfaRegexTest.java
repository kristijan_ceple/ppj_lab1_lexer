import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EnfaRegexTest {

	// The enfa that shall be tested is of the form: "a*ba*"
	
	Enfa testEnfa = new Enfa();
	
	@BeforeEach
	void setUpBeforeEach() {
		testEnfa = new Enfa();
		
		// Adding states
		Map<String, Boolean> states = new HashMap<>();
		testEnfa.addState("p1", false);
		testEnfa.setStartingState("p1");		// Let's set the starting Enfa.Automaton.State
		
		states.put("p2", false);
		states.put("p3", false);
		states.put("p4", true);
		testEnfa.addStates(states);
		
		// Setting the alphabet
		testEnfa.addAlphabetChar("a", "b");
		
		
		// Setting the transmissions
		Map<String, String[]> transMap = new HashMap<>();
		
		// Map for p1
		transMap.put("a", new String[] {"p2"});
		transMap.put(Enfa.EPSILON, new String[] {"p4"});
		testEnfa.addTransitions("p1", transMap);	// For p1
		
		// Map for p2
		transMap.put("a", new String[]{"p2"});
		transMap.put("b", new String[]{"p3"});
		testEnfa.addTransitions("p2", transMap);
		
		// Map for p3
		transMap.clear();
		transMap.put("a", new String[]{"p3"});
		transMap.put(Enfa.EPSILON, new String[]{"p4"});
		testEnfa.addTransitions("p3", transMap);
		
		// p4 has got no transitions - end-Enfa.Automaton.State
		
		// Prepare the Automaton for operation
		testEnfa.prepare();
	}
	
	@Test
	void EnfaTest() {
		// Let's make sure the actual structure is okay
		TreeSet<Enfa.Automaton.State> states = (TreeSet<Enfa.Automaton.State>)testEnfa.getStates();		// Want them sorted, so I can check alphabetically
		Enfa.Automaton.State startingState = testEnfa.getStartingState();
		
		// Let's first test the getStartingState function
		assertEquals(startingState.toString(), "p1");
		
		// Now it's time to test the other states(the structure of the automaton)
		Enfa.Automaton.State currState = states.first();
		assertEquals(testEnfa.getEmptyState().toString(), currState.toString());		// The empty Enfa.Automaton.State - means en error has occured in the input sequence - DEF NOT ACCEPTABLE
		
		currState = states.higher(currState);
		assertEquals("p1", currState.toString());
		
		currState = states.higher(currState);
		assertEquals("p2", currState.toString());
		
		currState = states.higher(currState);
		assertEquals("p3", currState.toString());
		
		currState = states.higher(currState);
		assertEquals("p4", currState.toString());
		
		// Now let's test the alphabet
		Set<Enfa.AlphabetChar> alphabetSet = testEnfa.getAlphabet();
		TreeSet<Enfa.AlphabetChar> sortedAlphabetSet = new TreeSet<Enfa.AlphabetChar>();
		sortedAlphabetSet.addAll(alphabetSet);
		
		Enfa.AlphabetChar current = sortedAlphabetSet.first();
		assertEquals(new Enfa.AlphabetChar(Enfa.EPSILON), current);
		
		current = sortedAlphabetSet.higher(current);
		assertEquals(new Enfa.AlphabetChar("a"), current);
		
		current = sortedAlphabetSet.higher(current);
		assertEquals(new Enfa.AlphabetChar("b"), current);
		
		// Transitions shall be tested in another test method
	}
	
	@Test
	void performSingleTransitionTestAccepted() {
		assertEquals(true, testEnfa.isAccepted());		// The Epsilon-trans to p1 means that even the empty input should be accepted
		
		testEnfa.performSingleTransition("a");
		assertEquals(true, testEnfa.isAccepted());
		
		// Let's check the destination states - they should be p4 and p2
		Set<Enfa.Automaton.State> activeStates = testEnfa.getActiveStates();
		assertEquals(2, activeStates.size());
		
		Set<String> expectedStateNames = new HashSet<String>();
		expectedStateNames.add("p4");
		expectedStateNames.add("p2");

		// Now let's check if the enfa really is in those states
		for(Enfa.Automaton.State currState : activeStates) {
			String name = currState.toString();
			assertTrue(expectedStateNames.contains(name));
		}
	}
	
	@Test
	void performSingleTransitionNotAccepted() {
		assertEquals(true, testEnfa.isAccepted());		// The Epsilon-trans to p1 means that even the empty input should be accepted
		
		testEnfa.performSingleTransition("b");
		assertEquals(false, testEnfa.isAccepted());
		
		// Let's check the destination states - that should be the empty Enfa.Automaton.State
		Set<Enfa.Automaton.State> activeStates = testEnfa.getActiveStates();
		assertEquals(1, activeStates.size());
		assertTrue(activeStates.contains(testEnfa.getEmptyState()));
	}
	
	@Test
	void isAcceptedTest() {
		assertEquals(true, testEnfa.isAccepted());		// Tests whether the initial Enfa.Automaton.State is acceptable(it shouldn't be)
	}

}
